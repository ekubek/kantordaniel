/*$.getJSON('http://api.nbp.pl/api/exchangerates/tables/A/', function (json) {

    var myJSON = json[0].rates;

    var currency = ["USD", "EUR", "HUF", "CHF", "GBP", "CZK", "HRK" ];

    var myTable = [];
    for (var x=0; x<currency.length; x++) {
        Object.keys( myJSON ).forEach( function( key ) {
            if( myJSON[key].code ===currency[x] ) {
                num = myJSON[key].mid + (myJSON[key].mid * 0.02);
                function roundNumber(num, dec) {
                    return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
                }
                myJSON[key].midSell = roundNumber(num,4);
                myTable.push(myJSON[key]);
            }
        })}

    $('#table').DataTable({
        paging: false,
        searching: false,
        ordering: false,
        pageLength: 10,
        info: false,
        data: myTable,
        columns: [
            {title: "waluta", "data": "currency" },
            {title: "kod", "data": "code"},
            {title: "sprzedaż", "data": "midSell"},
            {title: "kupno", "data": "mid"},
        ]
    });

});*/

$.getJSON('currency.json', function(json) {
    $('#table').DataTable({
        paging: false,
        searching: false,
        ordering: false,
        pageLength: 10,
        info: false,
        data: json,
        columns: [
            {title: "waluta", "data": "cur" },
            {title: "sprzedaż", "data": "sell"},
            {title: "kupno", "data": "buy"},
        ]
    });
});







